# run as sudo
echo 'deb http://linux-packages.resilio.com/resilio-sync/deb resilio-sync non-free' >> /etc/apt/sources.list.d/resilio-sync.list
wget -qO - https://linux-packages.resilio.com/resilio-sync/key.asc | sudo apt-key add -
apt-get update
apt-get install resilio-sync -y
sudo systemctl enable resilio-sync
sudo systemctl start resilio-sync
