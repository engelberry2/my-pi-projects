#!/usr/bin/python 
import sys
import RPi.GPIO as GPIO

# debugging
debug = True

# define GPIO object and ini GPIOs
all_pins = [2,3,4,17,27,22,10,9,11,18,23,24,25,8,7,5,6,12,13,16,19,20,21,26]

output_pins = {'OUT_act_relay':17}
input_pins = {'IN_PC_state':18}

def ini_pins():
    ### define funcs
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    debugprint("______________")
    debugprint("ini GPIO pins:")
    debugprint("--------------")
    resp = "[m_GPIO] initializing pins"
    
    for pin in all_pins:
        GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    
    for pin in output_pins:
        GPIO.setup(output_pins[pin], GPIO.OUT)
        GPIO.output(output_pins[pin], False)
        debugprint("OUT: " + pin + " -> turned off") 

    for pin in input_pins:
        GPIO.setup(input_pins[pin], GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        debugprint("IN: " + pin) 

    debugprint("--------------")
    
    return resp
    
def input(pin_str):
    try:
        return GPIO.input(input_pins[pin_str])
    except:
        debugprint("Error in input pin assignment of pin " + str(input_pins[pin_str]))
        
def output(pin_str,state): 
    try:
        GPIO.output(output_pins[pin_str],state)
    except:
        debugprint("Error in output pin assignment of pin " + str(output_pins[pin_str]))
  
def debugprint(message):
    if debug: print "[m_GPIO] " + message

### run if m_GPIO.py is called via terminal with a parameter and interprete first parameter
if len(sys.argv) > 1:
    parameter = str(sys.argv[1])
    debugprint("called moduel with parameter: " + parameter)
    if parameter == "ini_pins":
        ini_pins()
