import asyncore
import socket
import select
#import RPi.GPIO as GPIO
import time, json, m_GPIO
from datetime import datetime

class Client(asyncore.dispatcher_with_send):
    def __init__(self, socket=None, pollster=None):
        asyncore.dispatcher_with_send.__init__(self, socket)
        self.data = ''
        self.last_action = self.load_param("last_action")
        self.PC_state_color = ''
        self.com_sep = "#"
        self.color_green = "0,250,0"
        self.color_yellow = "250,250,0"
        self.color_red = "250,40,40"

        if pollster:
            self.pollster = pollster
            pollster.register(self, select.EPOLLIN)

    def handle_close(self):
        if self.pollster:
            self.pollster.unregister(self)

    def handle_read(self):
        receivedData = self.recv(8192)
        if not receivedData:
            self.close()
            return
        receivedData = self.data + receivedData
        while '\n' in receivedData:
            line, receivedData = receivedData.split('\n',1)
            self.handle_command(line)
        self.data = receivedData

    def handle_command(self, line):
        if line == 'app_send_ws':
            self.wakeup_suspend()
            self.update_PC_state()
            self.update_app() 
        elif line == 'app_send_update':  
            self.update_PC_state()
            self.update_app()   
        else:
            self.send('unknown command\n')
            print 'Unknown command:', line
        print line
        
    ###
    
    def save_param(self,param_name,value):
        filename = param_name+".json"
        with open(filename, 'w') as file:
            json.dump(value, file)
        file.closed
    
    def load_param(self,param):
        try:
            filename = param+".json"
            with open(filename, 'r') as file:
                result = json.load(file)
            file.closed
        except:
            result = "n/a"
        return result
        
    def wakeup_suspend(self):
        now = datetime.now()
        print "relay high"
        m_GPIO.output("OUT_act_relay",1)
        time.sleep(0.5)
        m_GPIO.output("OUT_act_relay",0)
        print "relay low"
        self.last_action = str(now.day)+"."+str(now.month)+"."+str(now.year)+" - "+str(now.hour)+":"+str(now.minute)+":"+str(now.second)
        self.save_param("last_action",self.last_action)
        self.update_app()
    
    def update_PC_state(self):
        if m_GPIO.input("IN_PC_state"):
            self.PC_state_color = self.color_green
        else:
            self.PC_state_color = self.color_red  
    
    def update_app(self):
        comstring = self.com_sep+"background."+self.PC_state_color+self.com_sep+self.last_action
        self.app_command(comstring)

    def app_command(self, command):
        self.send(command+"\n")

class Server(asyncore.dispatcher):
    def __init__(self, listen_to, pollster):
        asyncore.dispatcher.__init__(self)
        self.pollster = pollster
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.bind(listen_to)
        self.listen(5)

    def handle_accept(self):
        newSocket, address = self.accept()
        print "Connected from", address
        Client(newSocket,self.pollster)

def readwrite(obj, flags):
    try:
        if flags & select.EPOLLIN:
            obj.handle_read_event()
        if flags & select.EPOLLOUT:
            obj.handle_write_event()
        if flags & select.EPOLLPRI:
            obj.handle_expt_event()
        if flags & (select.EPOLLHUP | select.EPOLLERR | select.POLLNVAL):
            obj.handle_close()
    except socket.error, e:
        if e.args[0] not in asyncore._DISCONNECTED:
            obj.handle_error()
        else:
            obj.handle_close()
    except asyncore._reraised_exceptions:
        raise
    except:
        obj.handle_error()


class EPoll(object):
    def __init__(self):
        self.epoll = select.epoll()
        self.fdmap = {}
    def register(self, obj, flags):
        fd = obj.fileno()
        self.epoll.register(fd, flags)
        self.fdmap[fd] = obj
    def unregister(self, obj):
        fd = obj.fileno()
        del self.fdmap[fd]
        self.epoll.unregister(fd)
    def poll(self):
        evt = self.epoll.poll()
        for fd, flags in evt:
            yield self.fdmap[fd], flags


if __name__ == "__main__":
    pollster = EPoll()
    pollster.register(Server(("",54321),pollster), select.EPOLLIN)
    while True:
        evt = pollster.poll()
        for obj, flags in evt:
            readwrite(obj, flags)