#!/bin/bash
path="/home/webide/repositories/my-pi-projects/wakeuppc" 
case "$1" in
  start)
    echo "
    *** Starting engelberry services ***"
    echo "
    => starting noip2"
    sudo noip2
  
    echo "
    => initializing GPIOs"
    sudo python $path/m_GPIO.py ini_pins
    
    echo "
    => starting appserver"
    sudo bash $path/s_appserver.sh start
    echo "************** done **************"

    ;;
  stop)
    echo "
    *** Stopping appserver services ***"
    echo "
    => stopping appserver"
    sudo bash $path/s_appserver.sh stop
    echo "************** done **************"

   ;;
  status)
    echo "
    *** Stati of appserver services ***"
    echo "
    => niop2"
    sudo noip2 -S
    
    echo "
    => appserver"
    sudo bash $path/s_appserver.sh status
    echo "************** done **************"
   ;;
  *)
   echo "unkwown argument"
   exit 1
esac
exit 0
