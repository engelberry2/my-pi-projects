#!/bin/bash
path="/home/webide/repositories/my-pi-projects/wakeuppc/"
programm="appserver.py"
zdsock="zdsock_$programm"
params="-s $path$zdsock -z $path"

case "$1" in
start)
   echo "### Start $programm ###"
   sudo zdaemon -p "sudo python $path$programm" $params -d start
   ;;
stop)
   echo "### Stop $programm ###"
   sudo zdaemon -p "sudo python $path$programm" $params -d stop
   ;;
restart)
   echo "### Stop $programm ###"
   sudo zdaemon -p "sudo python $path$programm" $params -d stop
   echo "### Start $programm ###"
   sudo zdaemon -p "sudo python $path$programm" $params -d start
   ;;
status)
   echo "### Status of $programm ###"
   sudo zdaemon -p "sudo python $path$programm" $params -d status
   ;;
   *)
   echo "unkwown argument"
   exit 1
esac
exit 0
