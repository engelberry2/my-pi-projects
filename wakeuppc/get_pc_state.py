#!/usr/bin/python 
import time, m_GPIO, sys

# Debug ?
debug = False

# timining
record_duration = 5
   
### define funcs
def debug_print(text):
    if debug: print("[get_pc_state] " + text)

def record():
    current_state = m_GPIO.input('IN_PC_state')
    last_state = current_state
    t_start = time.time()
    t_last_toggle = t_start
    record = [(0,0)]
    while time.time() < t_start + record_duration
        current_state = m_GPIO.input('IN_PC_state')
        if current_state <> last_state:
            record.append((current_state,time.time() - t_last_toggle))
            t_last_toggle = time.time()
            last_state = current_state
    return record
 
def analyze(record):
    if len(record) >= min_blinks*2:
        state = "blink" 
